**Finger Recognition Computer Application**

This project is a computer application that allows users to perform a wide range of actions by simply raising fingers in the palm of their hand. The user can associate actions with a certain combination of fingers, choosing from a wide menu of predefined options or even copy other users' options. The application also includes games such as a car game, the ability to draw on the computer with their finger and save the drawing, and a game of rock, paper, and scissors.

**Technologies Used**

The project is built using the following technologies:


- Pygame
- Pyqt5
- OpenCV
- mediapipe

**Features**

The application allows the user to:

Perform actions by raising fingers in the palm of their hand
Associate actions with a certain combination of fingers
Choose from a wide menu of predefined options
Copy other users' options
Play games such as a car game and a game of rock, paper, and scissors
Draw on the computer with their finger and save the drawing
How it Works
The application uses computer vision techniques to detect and recognize the user's hand. Specifically, it separates the hand from the background by painting the hand white and the background black, then counts the transitions from black to white to detect the number of fingers raised. To improve the accuracy of finger recognition, the user is required to wear a blue glove on the palm of their hand.

**Getting Started**

To get started with the project, follow these steps:

Clone the repository: git clone https://gitlab.com/asafmagen28/ashdod-802-handcontrol.git
Run the application: python gui.py

**How to Use**

Upon launching the application, the user will be prompted to calibrate their hand. This involves showing their hand wearing the blue glove to the camera and having the application detect their fingers. Once calibrated, the user can begin raising fingers to perform actions.

To associate actions with certain combinations of fingers, the user can select from a wide menu of predefined options or create their own. They can also copy other users' options.

To play games, the user can select the game from the menu and follow the on-screen instructions.

To draw on the computer, the user can select the drawing option and use their finger to draw. They can save their drawing by pressing 's'.

**Contributors**

This project was created by Asaf Magen and Ilai Nevo.

**License**

This project is licensed under the MIT License - see the LICENSE file for details.
