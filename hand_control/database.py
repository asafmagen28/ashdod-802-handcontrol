import sqlite3
from sqlite3 import Error
import webbrowser
from ctypes import cast, POINTER
from comtypes import CLSCTX_ALL
from pycaw.pycaw import AudioUtilities, IAudioEndpointVolume
from gtts import gTTS
import os
from tkinter import Tk, Entry, Label, Button, messagebox
import tkinter as tk
import speech_recognition as sr
import pyperclip
from subprocess import run

# Connect to or create the database

ZERO_FINGERS = 0
ONE_FINGER = 1
TWO_FINGERS = 2
THREE_FINGERS = 3
FOUR_FINGERS = 4


def main():
    """
    the function used as a main function
    :return: none
    """

    print(check_if_user_exist('adsadsadasdsa'))
    insert_user_into_table('as', 'a', '0', '1', '2', '3', '4')
    #example
    """print(get_possible_commands())
    connection = connect_to_db()
    create_tables(connection)
    get_user_commands('a')
    commands = get_possible_commands()
    commands[0]()
    insert_commands_into_table()
    close_db(connection)"""

def connect_to_db():
    """
    the function connect the program to the database
    :return: none
    """
    try:
        connection = sqlite3.connect('menu.db')
        return connection
    except Error as e:
        print(e)

def take_action(username, user_command, connection=sqlite3.connect('menu.db')):
    """
    the function take_action takes the possible commands and use the user command to do the action the
    user wanted
    :param username: the current user username
    :param user_command: tne index of the user current command in the possible commands list
    :param connection: connection to the database
    :return: none
    """
    commands = get_possible_commands()
    commands[user_command]()

def get_user_commands(username, connection=sqlite3.connect('menu.db')):
    """
    the function returns the indexes of the user commands in the possible commands list
    :param username: the current user username
    :param connection: none
    :return: the function returns the indexes of the user commands in the possible commands list
    """
    # Create a cursor
    cursor = connection.cursor()
    sql = '''SELECT ZERO_FINGERS, ONE_FINGERS, TWO_FINGERS, THREE_FINGERS, FOUR_FINGERS FROM USERS WHERE USERNAME = "''' + username + '''"'''
    result = cursor.execute(sql).fetchall()
    return result
def create_tables(connection=sqlite3.connect('menu.db')):
    """
    the function create tables for the database, user's table and command's table
    :param connection: connectio to the database
    :return: none
    """
    # Create a cursor
    cursor = connection.cursor()

    # Execute the SQL command to create tables

    cursor.execute('''CREATE TABLE IF NOT EXISTS POSSIBLE_COMMANDS(
                    ID INTEGER PRIMARY KEY NOT NULL,
                    COMMAND_NAME TEXT NOT NULL,
                    UNIQUE(COMMAND_NAME)
                    );''')
    cursor.execute('''CREATE TABLE IF NOT EXISTS USERS
                 (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                 USERNAME TEXT NOT NULL,
                 PASSWORD TEXT NOT NULL,
                 ZERO_FINGERS INTEGER NOT NULL,
                 ONE_FINGERS INTEGER NOT NULL,
                 TWO_FINGERS INTEGER NOT NULL,
                 THREE_FINGERS INTEGER NOT NULL,
                 FOUR_FINGERS INTEGER NOT NULL,
                 FOREIGN KEY(ONE_FINGERS) REFERENCES ID(POSSIBLE_COMMANDS),
                 FOREIGN KEY(TWO_FINGERS) REFERENCES ID(POSSIBLE_COMMANDS),
                 FOREIGN KEY(THREE_FINGERS) REFERENCES ID(POSSIBLE_COMMANDS),
                 FOREIGN KEY(FOUR_FINGERS) REFERENCES ID(POSSIBLE_COMMANDS)
                 );''')

def copy_user_menu(curr_username,copied_username, connection=sqlite3.connect('menu.db')):
    """
    the funtion copied the geastures from one user to other
    :param curr_username: current user
    :param copied_username: the user that we copy from
    :param connection: connection to the database
    :return: none
    """
    if check_if_user_exist(curr_username) and check_if_user_exist(copied_username):
        copied_user_commands = get_user_commands(copied_username)
        update_commands(copied_user_commands[0][0], copied_user_commands[0][1], copied_user_commands[0][2],copied_user_commands[0][3],copied_user_commands[0][4], curr_username)

    else:
        print("Data does not exist in the database.")


def return_users(connection=sqlite3.connect('menu.db')):
    """
    the function returns all the users that exists in the software
    :param connection: connection to the database
    :return: returns all the users that exists in the software
    """
    cursor = connection.cursor()
    cursor.execute(
        '''SELECT username FROM USERS''')
    users_tuple = cursor.fetchall()
    users_list = [user[0] for user in users_tuple]
    if users_list:
        print("Data exists in the database.")
    else:
        print("Data does not exist in the database.")
    return users_list

def update_commands(command_zero, command_one, command_two, command_three, command_four, username, connection=sqlite3.connect('menu.db')):
    """
    the function updates the current user's commands in the database by indexes in the possible commands list
    :param command_zero: the index of the command that matches to zero fingers raised
    :param command_one: the index of the command that matches to one fingers raised
    :param command_two: the index of the command that matches to two fingers raised
    :param command_three: the index of the command that matches to three fingers raised
    :param command_four: the index of the command that matches to four fingers raised
    :param username: current user's username
    :param connection: connection to the database
    :return: if the commands updated successfully or not
    """

    cursor = connection.cursor()

    sql_update = '''UPDATE USERS SET ZERO_FINGERS = ''' + str(command_zero) + ''', ONE_FINGERS = ''' + str(command_one) + ''', TWO_FINGERS = ''' + str(command_two) +''', THREE_FINGERS = ''' + str(command_three) + ''', FOUR_FINGERS = ''' + str(command_four) + ''' WHERE USERNAME = "''' + username + '''"'''
    print('''UPDATE USERS SET ZERO_FINGERS = ''' + str(command_zero) + ''', ONE_FINGERS = ''' + str(command_one) + ''', TWO_FINGERS = ''' + str(command_two) +''', THREE_FINGERS = ''' + str(command_three) + ''', FOUR_FINGERS = ''' + str(command_four) + ''' WHERE USERNAME = "''' + username + '''"''')
    cursor.execute(sql_update)
    try:
        cursor.execute(sql_update)
        connection.commit()
        return True
    except Error as e:
        print(e)
        return False

def get_possible_commands():
    """
    the function returns array that includes the options of the menu which presents to the user in the menu of the program
    :return: array that includes the options of the menu which presents to the user in the menu of the program
    """
    possible_commands = [text_to_speech, enable_voice_commands, open_whatsapp, open_youtube, open_spotify, increase_volume, decrease_volume, set_mute, set_max_volume, race_car, draw_finger, rock_paper_scissors, keyboard_arrows_control]
    return possible_commands

def insert_commands_into_table(connection=sqlite3.connect('menu.db')):
    """
    the function insert the possible commands to the program menu - the commands table
    :param connection: connection to the database
    :return: none
    """
    possible_commands = get_possible_commands()
    cursor = connection.cursor()
    for command in possible_commands:
        sql_insert = '''
        INSERT INTO POSSIBLE_COMMANDS (COMMAND_NAME) VALUES("''' + command.__name__ + '''")'''
        try:
            cursor.execute(sql_insert)
            connection.commit()
        except Error as e:
            print(e)
    sql_insert = "SELECT * FROM POSSIBLE_COMMANDS"
    cursor.execute(sql_insert)
    cursor.fetchall()


def get_commands_table(connection=sqlite3.connect('menu.db')):
    """
    the function returns the possible commands from the DataBase
    :param connection: connection to the database
    :return:  the possible commands from the DataBase
    """
    cursor = connection.cursor()
    sql_insert = "SELECT * FROM POSSIBLE_COMMANDS"
    cursor.execute(sql_insert)
    return cursor.fetchall()

def valid_password(username, password, connection=sqlite3.connect('menu.db')):
    """
    the function checks if the username and the password match or not
    :param username: username to check
    :param password: password to check
    :param connection: connection to the database
    :return: if the username and the password match or not
    """
    cursor = connection.cursor()

    cursor.execute('''SELECT * FROM USERS WHERE USERNAME = "''' + username + '''" AND PASSWORD = "''' + password + '''"''')

    if cursor.fetchone():
        print("Data exists in the database.")
        return True
    else:
        print("Data does not exist in the database.")
        return False

def check_if_user_exist(username, connection=sqlite3.connect('menu.db')):
    """
    the function checks if the user is exists
    :param username: username to check the user
    :param connection: connection to the database
    :return: if the user exists or not
    """
    cursor = connection.cursor()
    sql_insert = '''SELECT * FROM USERS WHERE USERNAME = "''' + username + '''"'''
    print(sql_insert)
    cursor.execute(sql_insert)
    result = cursor.fetchall()
    print("result - " + str(result))

    if result:
        print("Data exists in the database.")
        return True
    else:
        print("Data does not exist in the database.")
        return False



def insert_user_into_table(username, password, zero_finger_command, one_finger_command, two_fingers_command, three_finger_command, four_fingers_command, connection=sqlite3.connect('menu.db')):
    """
    the function insert new user into the user's table
    :param username: new username
    :param password: new user password
    :param one_finger_command: one finger command - command's ID
    :param two_fingers_command: two fingers command - command's ID
    :param three_finger_command: three fingers command - command's ID
    :param four_fingers_command: four fingers command - command's ID
    :param connection: connection to the database
    :return: none
    """
    # Create a cursor
    cursor = connection.cursor()
    sql_insert = '''
    INSERT INTO USERS (USERNAME, PASSWORD, ZERO_FINGERS, ONE_FINGERS, TWO_FINGERS, THREE_FINGERS, FOUR_FINGERS) VALUES(
    "''' + username + '''", "''' + password + '''", ''' + zero_finger_command + ''', ''' + one_finger_command + ''', ''' + two_fingers_command + ''', ''' + three_finger_command + ''', ''' + four_fingers_command + ''')'''

    try:
        cursor.execute(sql_insert)
        connection.commit()
    except Error as e:
        print(e)


def delete_user_from_table(username, connection=sqlite3.connect('menu.db')):
    """
    the function deletes a given user from the user's table
    :param username: the user to remove
    :param connection: connection to the database
    :return: none
    """
    # Create a cursor
    cursor = connection.cursor()
    sql_insert = '''DELETE FROM USERS WHERE USERNAME = "''' + username + '''"'''
    try:
        cursor.execute(sql_insert)
        connection.commit()
    except Error as e:
        print(e)

def num_of_fingers_string(num_of_fingers):
    """
    the function returns the number of upper fingers in a string that matches to the database
    :param num_of_fingers: num of upper fingers to cast
    :return: the number of upper fingers in a string that matches to the database
    """
    if num_of_fingers == ONE_FINGER:
        return "ONE_FINGERS"
    elif num_of_fingers == TWO_FINGERS:
        return "TWO_FINGERS"
    elif num_of_fingers == THREE_FINGERS:
        return "THREE_FINGERS"
    elif num_of_fingers == FOUR_FINGERS:
        return "FOUR_FINGERS"
    else:
        return "ZERO_FINGERS"

def get_command_by_fingers(username, num_of_fingers, connection=sqlite3.connect('menu.db')):
    """
    selects the matched command according to the number of raised fingers and calls a function depend on the user's menu
    :param connection: connection to the database
    :param username: username of the current user
    :param num_of_fingers: num of upper fingers
    :return: none
    """
    cursor = connection.cursor()
    num_of_fingers_db = num_of_fingers_string(num_of_fingers)

    try:
        cursor.execute('''SELECT ''' + str(num_of_fingers_db) + ''' from USERS WHERE USERNAME = "''' + username + '''"''')
        command_id = cursor.fetchall()
        get_possible_commands()[command_id - 1]()
    except Error as e:
        print(e)

    close_db(connection)


def close_db(connection=sqlite3.connect('menu.db')):
    """
    the fucntion close the db
    :param connection: connection to the db
    :return: none
    """
    # Commit the changes and close the connection
    connection.commit()
    connection.close()

def open_popup():
    """
    the function open a pop-up window to get data from the user and returns the data
    :return: the data the user insert's
    """
    def on_submit():
        nonlocal result
        result = entry.get()
        root.destroy()
    result = None
    root = Tk()
    root.title("Enter text")
    label = Label(root, text="Please enter some text:")
    label.pack()
    entry = Entry(root)
    entry.pack()
    submit_button = Button(root, text="Submit", command=on_submit)
    submit_button.pack()
    root.mainloop()
    return result

def show_popup_and_save(text):
    """
    the fucntion show a pop-up message which includes the given text and copy the text to clipboard
    :param text: the text to show
    :return: none
    """
    root = tk.Tk()
    root.withdraw()
    tk.messagebox.showinfo("Text", text)
    root.destroy()
    pyperclip.copy(text)
    with open("voice_command.txt", "a") as file:
        file.write(text + "\n")
        #with open('voice_command.txt', 'w') as f:
          #  f.write(text)

def show_popup(text):
    """
    the fucntion show a pop-up message which includes the given text
    :param text: the text to show
    :return: none
    """
    root = tk.Tk()
    root.withdraw()
    tk.messagebox.showinfo("Text", text)
    root.destroy()


def open_youtube():
    """
    the function open 'youtube.com' in the browser
    :return: none
    """
    # Initialize the web driver
    webbrowser.open('https://www.youtube.com/')

def open_spotify():
    """
    the function open 'spotify.com' in the browser
    :return: none
    """
    # Initialize the web driver
    webbrowser.open('https://open.spotify.com/')

def open_whatsapp():
    """
    the function open 'whatsapp.com' in the browser
    :return: none
    """
    # Open the browser to the WhatsApp Web URL
    webbrowser.open("https://web.whatsapp.com/")


def increase_volume():
    """
    the function increase the volume in the computer by the same amount
    :return: none
    """
    devices = AudioUtilities.GetSpeakers()
    interface = devices.Activate(IAudioEndpointVolume._iid_, CLSCTX_ALL, None)
    volume = cast(interface, POINTER(IAudioEndpointVolume))
    current = volume.GetMasterVolumeLevel()
    volume.SetMute(0, None)
    try:
        volume.SetMasterVolumeLevel(current + 3.0, None)
    except:
        volume.SetMasterVolumeLevel(-0.0, None)

def decrease_volume():
    """
    the function decrease the volume in the computer by the same amount
    :return: none
    """
    devices = AudioUtilities.GetSpeakers()
    interface = devices.Activate(IAudioEndpointVolume._iid_, CLSCTX_ALL, None)
    volume = cast(interface, POINTER(IAudioEndpointVolume))
    current = volume.GetMasterVolumeLevel()
    try:
        volume.SetMasterVolumeLevel(current - 3.0, None)
    except:
        volume.SetMute(1, None)

def set_mute():
    """
    the function set the volume on mute
    :return:
    """
    devices = AudioUtilities.GetSpeakers()
    interface = devices.Activate(IAudioEndpointVolume._iid_, CLSCTX_ALL, None)
    volume = cast(interface, POINTER(IAudioEndpointVolume))
    volume.SetMute(1, None)

def set_max_volume():
    """
    the function set the volume on maximum volume
    :return:
    """
    devices = AudioUtilities.GetSpeakers()
    interface = devices.Activate(IAudioEndpointVolume._iid_, CLSCTX_ALL, None)
    volume = cast(interface, POINTER(IAudioEndpointVolume))
    volume.SetMasterVolumeLevel(-0.0, None)
    volume.SetMute(0, None)

def text_to_speech():
    """
    the function convert text to speech and save the file
    :return: none
    """
    text = open_popup()
    language = "en"
    try:
        tts = gTTS(text, lang=language)
        tts.save("output.mp3")
        os.system("start output.mp3")
    except:
        messagebox.showerror("Error", "Invalid data")
def rock_paper_scissors():
    """
    the function start rock-paper-scissors game
    :return:
    """
    run(["python", "rock-paper-scissors.py"])
def race_car():
    """
    the function start game
    :return: none
    """
    run(["python", "hand_control_car_game.py"])

def draw_finger():
    """
    the function start game
    :return: none
    """
    run(["python", "draw_using_hand.py"])

def keyboard_arrows_control():
    """
    the function calls the keyboard_arrows_control mode to control the arrows by using the fingers
    :return: none
    """
    run(["python", "keyboard_arrows_mode.py"])
def enable_voice_commands():
    """
    the function recognise voice and call a function to save the data in a txt file and copy it to the clipboard
    :return: none
    """
    # create recognizer and mic instances
    recognizer = sr.Recognizer()
    microphone = sr.Microphone()
    # adjust the recognizer sensitivity to ambient noise and record audio
    with microphone as source:
        recognizer.adjust_for_ambient_noise(source)
        audio = recognizer.listen(source)

    # recognize speech using Google Speech Recognition
    try:
        command = recognizer.recognize_google(audio)
        show_popup_and_save(command)
        print("Command: " + command)
    except sr.UnknownValueError:
        show_popup("Google Speech Recognition could not understand audio")
    except sr.RequestError as e:
        show_popup("Could not request results from Google Speech Recognition service; {0}".format(e))



if __name__ == "__main__":
    main()
