# import the opencv library
import cv2
import numpy as np
import time

WHITE = 255
BLACK = 0
DIFFERENCE_DIVIDE = 10
OPT_ONE_FINGER = 2
WIDTH_START = 322
WIDTH_END = 598

TIMER_SET = 5
HEIGHT_START = 102
HEIGHT_END = 438

def main():
    print(run_camera(TIMER_SET))
def timer(n):
    start = time.time()
    while True:
        if time.time() - start >= n:
            return True
    return False
def run_camera(timeToScan):
    # define a video capture object
    w, h = 1920, 1080
    vid = cv2.VideoCapture(0)
    vid.set(3, 1080)
    vid.set(4, 1920)

    # print height and width
    
    width = vid.get(cv2.CAP_PROP_FRAME_WIDTH)
    height = vid.get(cv2.CAP_PROP_FRAME_HEIGHT)
    print(width, height)
    start = time.time()
    while (True):
        if time.time() - start >= timeToScan:
            # Set the timer for 3 seconds
            # After the loop release the cap object
            vid.release()
            # Destroy all the windows
            cv2.destroyAllWindows()
            return data
        # Capture the video frame
        # by frame
        ret, frame = vid.read()
        flipped_frame = cv2.flip(frame, 1)
        cv2.rectangle(flipped_frame, (320, 100), (600, 440), (0, 255, 0), 2)
        # Display the resulting frame

        #display timer
        cv2.putText(flipped_frame, str(format(TIMER_SET - (time.time() - start), '.2f')), (10, 100), cv2.FONT_HERSHEY_SIMPLEX,
                    2, (255, 255, 255), 10)

        # save the hand area in array
        #take this range to ignore the rectangle draw
        hand_area = flipped_frame[HEIGHT_START:HEIGHT_END, WIDTH_START:WIDTH_END]

        # Convert the frame to the HSV color space
        hsv = cv2.cvtColor(hand_area, cv2.COLOR_BGR2HSV)

        # Define the range of the color green in HSV
        lower_blue = np.array([100, 50, 50])
        upper_blue = np.array([130, 255, 255])

        # Threshold the frame to get only green colors
        mask = cv2.inRange(hsv, lower_blue, upper_blue)

        # Find the contours in the frame
        res = cv2.bitwise_and(hand_area, hand_area, mask=mask)
        # Show the frame
        cv2.imshow("blue frame", mask)


        width_hand = WIDTH_END - WIDTH_START
        height_hand = HEIGHT_END - HEIGHT_START
        difference = height_hand / DIFFERENCE_DIVIDE
        highest = 0
        difference_between_pixels = 0
        points_recognized = []
        len_between_edges = []

        #loop througth the frame and to count the number of upper fingers using he difference between the pixels
        for y in range(0, height_hand, int(difference)):
            counter = 0
            difference_between_pixels = 0
            curr_pixel = mask[y, 0]
            for x in range(width_hand):
                # checks if the previous and the current pixels are different
                if mask[y, x] != curr_pixel:
                    counter = counter + 1
                elif curr_pixel == WHITE and mask[y, x] == WHITE:
                    difference_between_pixels += 1
                curr_pixel = mask[y, x]
            if (counter > 1):
                points_recognized.append(counter)
            if counter == OPT_ONE_FINGER:
                len_between_edges.append(difference_between_pixels)


        try:
            #checks if it's recognized only one finger or closed hand
            if int(points_recognized[1] / 2) == 1:
                #checks the ratio between the parts in the hands to check if raised one or zero fingers
                if (max(len_between_edges) / len_between_edges[1]) > 3:
                    cv2.putText(flipped_frame, str(1), (45, 375), cv2.FONT_HERSHEY_PLAIN,
                                10, (255, 0, 0), 20)
                    data = 1
                else:
                    cv2.putText(flipped_frame, str(0), (45, 375), cv2.FONT_HERSHEY_PLAIN,
                                10, (255, 0, 0), 20)
                    data = 0
            else:
                cv2.putText(flipped_frame, str(int(points_recognized[2] / 2)), (45, 375), cv2.FONT_HERSHEY_PLAIN,
                            10, (255, 0, 0), 20)
                #cv2.putText(flipped_frame, str(int(max(points_recognized) / 2)), (45, 375), cv2.FONT_HERSHEY_PLAIN,
                          # 10, (255, 0, 0), 20)

                #handle with number bigger than 4
                if int(max(points_recognized) / 2) > 4:
                    data = 4
                else:
                    data = int(points_recognized[2] / 2)
        except:
            data = -1
        cv2.imshow('original frame', flipped_frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break


    # After the loop release the cap object
    vid.release()
    # Destroy all the windows
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()
