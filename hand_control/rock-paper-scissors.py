import sys
sys.path.append('C:/Users/user/Documents/Magshimim/12grade/ashdod-802-handcontrol/hand_control/venv/Lib/site-packages')

import finger_detection_using_glove
import random
from PIL import Image
import time
from PIL import Image, ImageDraw, ImageFont, ImageTk
import tkinter as tk
from tkinter import messagebox

from PyQt5.QtWidgets import QDialog, QStackedWidget, QApplication, QMainWindow, QApplication, QLabel, QTextEdit, QPushButton, QPlainTextEdit, QMenuBar
from PyQt5 import uic
from PyQt5 import QtWidgets
import sys
import pygame

rounds_to_win = 0
TIME_TO_SCAN = 5
app = QApplication(sys.argv)

ROCK = 0
PAPER = 4
SCISSORS = 2
COMPUTER_OPTIONS = [ROCK, PAPER, SCISSORS]
POSSIBLE_NUM_OF_ROUNDS = [1,2,3,4,5,6,7,8,9]
USER_WIN = 1
COMPUTER_WIN = 0


class RockPaperScissorsScreen(QDialog):
    def __init__(self):
        super(RockPaperScissorsScreen, self).__init__()
        #load the ui file
        ui_loaded = uic.loadUi("rock-paper-scissors_ui.ui", self)

        for round in POSSIBLE_NUM_OF_ROUNDS:
            self.num_of_rounds.addItem(str(round))

        self.start.clicked.connect(self.startfunction)
        self.quit.clicked.connect(self.closefunction)

    def startfunction(self):
        winner = start_game(int(self.num_of_rounds.currentIndex() + 1))
        if winner == USER_WIN:
            messagebox.showinfo("You won the game!!", "User win game!")
        else:
            messagebox.showinfo("You lost...:(", "User lose game :(")
        global app
        sys.exit(app.exec_())

    def closefunction(self):
        global app
        sys.exit(app.exec_())



def main():
    global app
    welcome = RockPaperScissorsScreen()
    widget = QStackedWidget()
    widget.addWidget(welcome)
    widget.setFixedWidth(600)
    widget.setFixedHeight(600)
    widget.show()

    try:
        sys.exit(app.exec_())
    except:
        print("Exiting")

def show_images(user, computer):
    # Load the two images
    user_image = Image.open(user)
    computer_image = Image.open(computer)

    # Create the window
    window = tk.Tk()

    window.title("Rock-Paper-Scissors")
    window.geometry(f"{user_image.width * 2}x{user_image.height}")

    # Define the font and text size for the labels
    font = ImageFont.truetype("arial.ttf", 20)

    # Add the "User" and "Computer" labels to each image
    user_draw = ImageDraw.Draw(user_image)
    user_draw.text((10, 10), "User", font=font, fill="black")
    computer_draw = ImageDraw.Draw(computer_image)
    computer_draw.text((10, 10), "Computer", font=font, fill="black")

    # Create the image labels
    user_label = tk.Label(window)
    computer_label = tk.Label(window)

    # Display the images and update the window
    tk_user_image = ImageTk.PhotoImage(user_image)
    tk_computer_image = ImageTk.PhotoImage(computer_image)
    user_label.config(image=tk_user_image)
    computer_label.config(image=tk_computer_image)
    user_label.pack(side=tk.LEFT)
    computer_label.pack(side=tk.RIGHT)
    window.update()

    # Wait for 3 seconds
    time.sleep(3)

    # Close the window
    window.destroy()


def start_game(rounds_to_win_from_user):
    """
    the function starts the game
    :param rounds_to_win_from_user: number of rounds to pley
    :return: the winner
    """
    global rounds_to_win
    rounds_to_win = rounds_to_win_from_user

    user_wins = 0
    computer_wins = 0

    # Load the image
    rock_image = "rock.jpg"
    paper_image = "paper.jpg"
    scissors_image = "scissors.jpg"
    user_image = 0
    computer_image = 0

    while user_wins < rounds_to_win and computer_wins < rounds_to_win:
        flag = True
        user_choice = finger_detection_using_glove.run_camera(TIME_TO_SCAN)
        computer_choice = random.choice(COMPUTER_OPTIONS)

        if user_choice == ROCK:
            user_image = rock_image
        elif user_choice == SCISSORS:
            user_image = scissors_image
        elif user_choice >= PAPER:
            user_image = paper_image
        else:
            flag = False

        if computer_choice == ROCK:
            computer_image = rock_image
        elif computer_choice == SCISSORS:
            computer_image = scissors_image
        else:
            computer_image = paper_image

        if flag:
            show_images(user_image, computer_image)

            root = tk.Tk()
            root.withdraw()
            if user_choice == computer_choice:
                messagebox.showinfo("Tie!",
                """tie!
current results
user: {}
computer: {}""".format(user_wins, computer_wins))
                root.destroy()


            elif (user_choice == ROCK and computer_choice == SCISSORS) or \
                     (user_choice == PAPER and computer_choice == ROCK) or \
                     (user_choice == SCISSORS and computer_choice == PAPER):
                user_wins += 1
                messagebox.showinfo("User win!",
                """
User wins current round!
current results:
user: {}
computer: {}
                """.format(user_wins, computer_wins))

                # Destroy the root window
                root.destroy()

            else:
                computer_wins += 1
                messagebox.showinfo("Computer win!",
                """
Computer wins current round!
current results:
user: {}
computer: {}
""".format(user_wins, computer_wins))

                # Destroy the root window
                root.destroy()

    if user_wins > computer_wins:
        return USER_WIN
    else:
        return COMPUTER_WIN

if __name__ == '__main__':
    main()