﻿using UnityEngine;
using OpenCVForUnity;

public class FingerCounter : MonoBehaviour
{
    const int WHITE = 255;
    const int BLACK = 0;
    const int DIFFERENCE_DIVIDE = 16;
    const int OPT_ONE_FINGER = 2;
    const int WIDTH_START = 322;
    const int WIDTH_END = 598;
    const int HEIGHT_START = 102;
    const int HEIGHT_END = 438;

    int width, height;
    VideoCapture vid;

    private void Start()
    {
        width = 1920;
        height = 1080;

        vid = new VideoCapture(0);
        vid.width = 1080;
        vid.height = 1920;

        Debug.Log("Width: " + vid.width + " Height: " + vid.height);
    }

    private void Update()
    {
        Mat frame = new Mat();
        if (vid.isOpened())
        {
            vid.read(frame);

            Imgproc.rectangle(frame, new Point(320, 100), new Point(600, 440), new Scalar(0, 255, 0), 2);

            Mat hand_area = frame.submat(HEIGHT_START, HEIGHT_END, WIDTH_START, WIDTH_END);

            Mat hsv = new Mat();
            Imgproc.cvtColor(hand_area, hsv, Imgproc.COLOR_BGR2HSV);

            Scalar lower_blue = new Scalar(90, 50, 50);
            Scalar upper_blue = new Scalar(130, 255, 255);

            Mat mask = new Mat();
            Core.inRange(hsv, lower_blue, upper_blue, mask);

            Mat res = new Mat();
            Core.bitwise_and(hand_area, hand_area, res, mask);

            int width_hand = WIDTH_END - WIDTH_START;
            int height_hand = HEIGHT_END - HEIGHT_START;
            int difference = height_hand / DIFFERENCE_DIVIDE;
            int highest = 0;
            int difference_between_pixels = 0;
            int[] points_recognized = new int[0];
            int[] len_between_edges = new int[0];

            for (int y = 0; y < height_hand; y += difference)
            {
                int counter = 0;
                difference_between_pixels = 0;
                int curr_pixel = (int)mask.get(y, 0)[0];

                for (int x = 0; x < width_hand; x++)
                {
                    if ((int)mask.get(y, x)[0] != curr_pixel)
                    {
                        counter++;
                    }
                    else if (curr_pixel == WHITE && (int)mask.get(y, x)[0] == WHITE)
                    {
                        difference_between_pixels++;
                    }

                    curr_pixel = (int)mask.get(y, x)[0];
                }

                if (counter > 1)
                {
                    points_recognized.Add(counter);
                }

                if (counter == 2)
                {
                    len_between_edges.Add(difference_between_pixels);
                }

                Console.WriteLine(string.Join(", ", len_between_edges));

                try
                {
                    Console.WriteLine("max: " + len_between_edges.Max());
                }
                catch
                {
                    Console.WriteLine("none");
                }

                try
                {
                    if (points_recognized[2] / 2 == 1)
                    {
                        if (len_between_edges.Max() - len_between_edges[1] > 40)
                        {
                            Cv2.PutText(frame, "1", new Point(45, 375), HersheyFonts.HersheyPlain, 10, new Scalar(255, 0, 0), 20);
                        }
                        else
                        {
                            Cv2.PutText(frame, "0", new Point(45, 375), HersheyFonts.HersheyPlain, 10, new Scalar(255, 0, 0), 20);
                        }
                    }
                    else
                    {
                        Cv2.PutText(frame, (points_recognized[2] / 2).ToString(), new Point(45, 375), HersheyFonts.HersheyPlain, 10, new Scalar(255, 0, 0), 20);
                    }
                }
                catch
                {
                    Cv2.PutText(frame, "len: " + points_recognized.Count, new Point(45, 375), HersheyFonts.HersheyPlain, 10, new Scalar(255, 0, 0), 20);
                }

                Cv2.ImShow("original frame", frame);

                if (Cv2.WaitKey(1) & 0xff == 'q')
                {
                    break;
                }

                vid.Release();
                Cv2.DestroyAllWindows();



