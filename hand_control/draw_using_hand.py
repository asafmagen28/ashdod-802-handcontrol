import sys
sys.path.append('C:/Users/user/Documents/Magshimim/12grade/ashdod-802-handcontrol/hand_control/venv/Lib/site-packages')

import tkinter as tk
import cv2
import mediapipe as mp
from tkinter import messagebox


# Initialize Mediapipe Finger Detection
mp_drawing = mp.solutions.drawing_utils
mp_hands = mp.solutions.hands


# Create a tkinter root window
root = tk.Tk()
root.withdraw()

# Show the pop-up message
messagebox.showwarning("Glove Alert", "Please remove your glove to draw")

# Destroy the root window
root.destroy()

# Initialize OpenCV window
cv2.namedWindow("Finger Drawing", cv2.WINDOW_NORMAL)

# Set up drawing parameters
drawing_color = (0, 255, 0)
drawing_thickness = 5

# Start capturing video
cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)

# Initialize finger tip coordinates
finger_tip_coords = []


# Loop over frames
while True:
    # Read frame from video capture
    ret, frame = cap.read()
    if not ret:
        break

    # Flip frame horizontally
    frame = cv2.flip(frame, 1)

    # Convert frame to RGB format
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

    # Add "Press c to clear" text in the top-left corner of the frame
    cv2.putText(frame, "Press 'c' to clear, 's' to save, 'e' for exit", (10, 30),
                cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 255), 2)

    # Process frame with Mediapipe finger detection
    with mp_hands.Hands(min_detection_confidence=0.5, min_tracking_confidence=0.5) as hands:
        results = hands.process(frame)
        if results.multi_hand_landmarks:
            # Get index finger tip coordinates
            hand_landmarks = results.multi_hand_landmarks[0]
            index_finger_tip = hand_landmarks.landmark[mp_hands.HandLandmark.INDEX_FINGER_TIP]
            index_finger_tip_x = int(index_finger_tip.x * frame.shape[1])
            index_finger_tip_y = int(index_finger_tip.y * frame.shape[0])

            # Add finger tip coordinates to list
            finger_tip_coords.append((index_finger_tip_x, index_finger_tip_y))

            # Draw line on screen from previous finger tip to current finger tip
        if len(finger_tip_coords) > 1:
            for i in range(len(finger_tip_coords) - 1):
                cv2.line(frame, finger_tip_coords[i], finger_tip_coords[i + 1], drawing_color, drawing_thickness)


    # Convert frame back to BGR format
    frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)

    # Show finger tip coordinates on screen
    for coord in finger_tip_coords:
        cv2.circle(frame, coord, 5, drawing_color, -1)

    # Show frame in OpenCV window
    cv2.imshow("Finger Drawing", frame)

    # Check for key press
    key = cv2.waitKey(1)
    if key == ord("e"):
        break
    elif key == ord("c"):
        # Clear finger tip coordinates and drawing
        finger_tip_coords = []
        cv2.destroyAllWindows()
        cv2.namedWindow("Finger Drawing", cv2.WINDOW_NORMAL)
    elif key == ord("s"):
        # Save finger drawing as image file
        file_name = "finger_drawing.png"

        # Create a copy of the current frame with only the finger drawing
        drawing_frame = frame.copy()
        drawing_frame[:] = (0, 0, 0)  # Set background to black
        for i in range(len(finger_tip_coords) - 1):
            cv2.line(drawing_frame, finger_tip_coords[i], finger_tip_coords[i + 1], drawing_color, drawing_thickness)

        cv2.imwrite(file_name, drawing_frame)

        root = tk.Tk()
        root.withdraw()

        messagebox.showinfo("File Saved", f"Finger drawing saved as '{file_name}'")
        root.destroy()

# Release video capture and close OpenCV window
cap.release()
cv2.destroyAllWindows()
