# import the opencv library
import sys
sys.path.append('C:/Users/user/Documents/Magshimim/12grade/ashdod-802-handcontrol/hand_control/venv/Lib/site-packages')

import cv2
import numpy as np
from pygame.locals import *
import pygame
import random

WHITE = 255
BLACK = 0
DIFFERENCE_DIVIDE = 14
OPT_ONE_FINGER = 2
WIDTH_START = 322
WIDTH_END = 598

HEIGHT_START = 102
HEIGHT_END = 438

NUM_FRAME_TO_DECIDE = 25
RIGHT_FINGERS = 1
LEFT_FINGERS = 2
def main():
    car_game()

def car_game():
    pygame.init()
    LEFT = pygame.USEREVENT + 1
    RIGHT = pygame.USEREVENT + 2
    #QUIT = pygame.USEREVENT + 1
    # create the window
    width = 500
    height = 500
    screen_size = (width, height)
    screen = pygame.display.set_mode(screen_size)
    pygame.display.set_caption('Car Game')

    # colors
    gray = (100, 100, 100)
    green = (76, 208, 56)
    red = (200, 0, 0)
    white = (255, 255, 255)
    yellow = (255, 232, 0)

    # road and marker sizes
    road_width = 300
    marker_width = 10
    marker_height = 50

    # lane coordinates
    left_lane = 150
    center_lane = 250
    right_lane = 350
    lanes = [left_lane, center_lane, right_lane]

    # road and edge markers
    road = (100, 0, road_width, height)
    left_edge_marker = (95, 0, marker_width, height)
    right_edge_marker = (395, 0, marker_width, height)

    # for animating movement of the lane markers
    lane_marker_move_y = 0

    # player's starting coordinates
    player_x = 250
    player_y = 400

    # frame settings
    clock = pygame.time.Clock()
    fps = 120

    # game settings
    gameover = False
    speed = 4
    score = 0

    class Vehicle(pygame.sprite.Sprite):

        def __init__(self, image, x, y):
            pygame.sprite.Sprite.__init__(self)

            # scale the image down so it's not wider than the lane
            image_scale = 45 / image.get_rect().width
            new_width = image.get_rect().width * image_scale
            new_height = image.get_rect().height * image_scale
            self.image = pygame.transform.scale(image, (new_width, new_height))

            self.rect = self.image.get_rect()
            self.rect.center = [x, y]

    class PlayerVehicle(Vehicle):

        def __init__(self, x, y):
            image = pygame.image.load('images/car.png')
            super().__init__(image, x, y)

    # sprite groups
    player_group = pygame.sprite.Group()
    vehicle_group = pygame.sprite.Group()

    # create the player's car
    player = PlayerVehicle(player_x, player_y)
    player_group.add(player)

    # load the vehicle images
    image_filenames = ['pickup_truck.png', 'semi_trailer.png', 'taxi.png', 'van.png']
    vehicle_images = []
    for image_filename in image_filenames:
        image = pygame.image.load('images/' + image_filename)
        vehicle_images.append(image)

    # load the crash image
    crash = pygame.image.load('images/crash.png')
    crash_rect = crash.get_rect()
    # game loop

    # define a video capture object
    w, h = 1920, 1080
    vid = cv2.VideoCapture(0)
    vid.set(3, 1080)
    vid.set(4, 1920)

    # print height and width

    width_cam = vid.get(cv2.CAP_PROP_FRAME_WIDTH)
    height_cam = vid.get(cv2.CAP_PROP_FRAME_HEIGHT)
    print(width_cam, height_cam)

    points_recognized = [0,0,0,0]

    running = True
    count_frames = 0
    data_temp = 0
    while running:

        try:
            public_data = int(points_recognized[2] / 2)
        except:
            public_data = 0
        # Capture the video frame
        # by frame
        ret, frame = vid.read()
        flipped_frame = cv2.flip(frame, 1)
        cv2.rectangle(flipped_frame, (320, 100), (600, 440), (0, 255, 0), 2)
        # Display the resulting frame

        # save the hand area in array
        # take this range to ignore the rectangle draw
        hand_area = flipped_frame[HEIGHT_START:HEIGHT_END, WIDTH_START:WIDTH_END]

        # Convert the frame to the HSV color space
        hsv = cv2.cvtColor(hand_area, cv2.COLOR_BGR2HSV)

        # Define the range of the color green in HSV
        lower_blue = np.array([100, 50, 50])
        upper_blue = np.array([130, 255, 255])

        # Threshold the frame to get only green colors
        mask = cv2.inRange(hsv, lower_blue, upper_blue)

        # Find the contours in the frame
        res = cv2.bitwise_and(hand_area, hand_area, mask=mask)
        # Show the frame
        cv2.imshow("blue frame", mask)

        width_hand = WIDTH_END - WIDTH_START
        height_hand = HEIGHT_END - HEIGHT_START
        difference = height_hand / DIFFERENCE_DIVIDE
        highest = 0
        difference_between_pixels = 0
        points_recognized = []
        len_between_edges = []

        # loop througth the frame and to count the number of upper fingers using he difference between the pixels
        for y in range(0, height_hand, int(difference)):
            counter = 0
            difference_between_pixels = 0
            curr_pixel = mask[y, 0]
            for x in range(width_hand):
                # checks if the previous and the current pixels are different
                if mask[y, x] != curr_pixel:
                    counter = counter + 1
                elif curr_pixel == WHITE and mask[y, x] == WHITE:
                    difference_between_pixels += 1
                curr_pixel = mask[y, x]
            if (counter > 1):
                points_recognized.append(counter)
            if counter == OPT_ONE_FINGER:
                len_between_edges.append(difference_between_pixels)


        data = 0
        try:
            if int(points_recognized[2] / 2) == 1:
                if (max(len_between_edges) / len_between_edges[1]) > 3:
                    # print("1 - " + str(max(len_between_edges) / len_between_edges[1]))
                    cv2.putText(flipped_frame, str(1), (45, 375), cv2.FONT_HERSHEY_PLAIN,
                                10, (255, 0, 0), 20)
                    data = 1
                else:
                    # print("0 - " + str(max(len_between_edges) / len_between_edges[1]))
                    cv2.putText(flipped_frame, str(0), (45, 375), cv2.FONT_HERSHEY_PLAIN,
                                10, (255, 0, 0), 20)
                    data = 0
            else:
                cv2.putText(flipped_frame, str(int(points_recognized[2] / 2)), (45, 375), cv2.FONT_HERSHEY_PLAIN,
                            10, (255, 0, 0), 20)
                data = int(points_recognized[2] / 2)

        except:
            cv2.putText(flipped_frame, "len:" + str(len(points_recognized)), (45, 375), cv2.FONT_HERSHEY_PLAIN,
                        10, (255, 0, 0), 20)
        cv2.imshow('original frame', flipped_frame)
        print('data = ' + str(data))
        if data == RIGHT_FINGERS and data == data_temp:
            count_frames += 1
        elif data == LEFT_FINGERS and data == data_temp:
            count_frames += 1
        else:
            count_frames = 0
        data_temp = data

        if count_frames == NUM_FRAME_TO_DECIDE:
            if data == 1:
                pygame.event.post(pygame.event.Event(RIGHT))
            else:
                pygame.event.post(pygame.event.Event(LEFT))
            count_frames = 0


        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

        clock.tick(fps)

        for event in pygame.event.get():
            if event.type == QUIT:
                running = False

            if event.type == LEFT and player.rect.center[0] > left_lane:
                player.rect.x -= 100
            elif event.type == RIGHT and player.rect.center[0] < right_lane:
                player.rect.x += 100

            # check if there's a side swipe collision after changing lanes
            for vehicle in vehicle_group:
                if pygame.sprite.collide_rect(player, vehicle):

                    gameover = True

                    # place the player's car next to other vehicle
                    # and determine where to position the crash image
                    if event.type == LEFT:
                        player.rect.left = vehicle.rect.right
                        crash_rect.center = [player.rect.left, (player.rect.center[1] + vehicle.rect.center[1]) / 2]
                    elif event.type == RIGHT:
                        player.rect.right = vehicle.rect.left
                        crash_rect.center = [player.rect.right, (player.rect.center[1] + vehicle.rect.center[1]) / 2]

        # draw the grass
        screen.fill(green)

        # draw the road
        pygame.draw.rect(screen, gray, road)

        # draw the edge markers
        pygame.draw.rect(screen, yellow, left_edge_marker)
        pygame.draw.rect(screen, yellow, right_edge_marker)

        # draw the lane markers
        lane_marker_move_y += speed * 2
        if lane_marker_move_y >= marker_height * 2:
            lane_marker_move_y = 0
        for y in range(marker_height * -2, height, marker_height * 2):
            pygame.draw.rect(screen, white, (left_lane + 45, y + lane_marker_move_y, marker_width, marker_height))
            pygame.draw.rect(screen, white, (center_lane + 45, y + lane_marker_move_y, marker_width, marker_height))

        # draw the player's car
        player_group.draw(screen)

        # add a vehicle
        if len(vehicle_group) < 2:

            # ensure there's enough gap between vehicles
            add_vehicle = True
            for vehicle in vehicle_group:
                if vehicle.rect.top < vehicle.rect.height * 1.5:
                    add_vehicle = False

            if add_vehicle:
                # select a random lane
                lane = random.choice(lanes)

                # select a random vehicle image
                image = random.choice(vehicle_images)
                vehicle = Vehicle(image, lane, height / -2)
                vehicle_group.add(vehicle)

        # make the vehicles move
        for vehicle in vehicle_group:
            vehicle.rect.y += speed

            # remove vehicle once it goes off screen
            if vehicle.rect.top >= height:
                vehicle.kill()

                # add to score
                score += 1

                # speed up the game after passing 5 vehicles
                if score > 0 and score % 5 == 0:
                    speed += 1

        # draw the vehicles
        vehicle_group.draw(screen)

        # display the score
        font = pygame.font.Font(pygame.font.get_default_font(), 16)
        text = font.render('Score: ' + str(score), True, white)
        text_rect = text.get_rect()
        text_rect.center = (50, 400)
        screen.blit(text, text_rect)

        # display the rules
        font = pygame.font.Font(pygame.font.get_default_font(), 16)
        text = font.render('1 to right', True, white)
        text_rect = text.get_rect()
        text_rect.center = (50, 310)
        screen.blit(text, text_rect)

        # display the rules
        font = pygame.font.Font(pygame.font.get_default_font(), 16)
        text = font.render('2 to left', True, white)
        text_rect = text.get_rect()
        text_rect.center = (50, 350)
        screen.blit(text, text_rect)

        # check if there's a head on collision
        if pygame.sprite.spritecollide(player, vehicle_group, True):
            gameover = True
            crash_rect.center = [player.rect.center[0], player.rect.top]

        # display game over
        if gameover:
            screen.blit(crash, crash_rect)

            pygame.draw.rect(screen, red, (0, 50, width, 100))

            font = pygame.font.Font(pygame.font.get_default_font(), 16)
            text = font.render('Game over. Play again? (Enter Y or N)', True, white)
            text_rect = text.get_rect()
            text_rect.center = (width / 2, 100)
            screen.blit(text, text_rect)

        pygame.display.update()

        # wait for user's input to play again or exit
        while gameover:

            clock.tick(fps)

            for event in pygame.event.get():

                if event.type == QUIT:
                    gameover = False
                    running = False

                # get the user's input (y or n)
                if event.type == KEYDOWN:
                    if event.key == K_y:
                        # reset the game
                        gameover = False
                        speed = 2
                        score = 0
                        vehicle_group.empty()
                        player.rect.center = [player_x, player_y]
                    elif event.key == K_n:
                        # exit the loops
                        gameover = False
                        running = False

    pygame.quit()
    # After the loop release the cap object
    vid.release()
    # Destroy all the windows
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
