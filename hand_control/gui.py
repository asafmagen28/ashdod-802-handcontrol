from PyQt5.QtWidgets import QDialog, QStackedWidget, QApplication, QMainWindow, QApplication, QLabel, QTextEdit, QPushButton, QPlainTextEdit, QMenuBar
from PyQt5 import uic
from PyQt5 import QtWidgets
import sys
import database
import finger_detection_using_glove
import pygame

import cv2

#to tells that the camera is currently running
flag = False
USER_WIN = 1
COMPUTER_WIN = 0


class WelcomeScreen(QDialog):

    def __init__(self):
        super(WelcomeScreen, self).__init__()
        #load the ui file
        uic.loadUi("welcome.ui", self)
        self.login.clicked.connect(self.gotologin)
        self.signup.clicked.connect(self.gotosignup)
        database.create_tables()

    def gotologin(self):
        login = LoginScreen()
        widget.addWidget(login)
        widget.setCurrentIndex(widget.currentIndex()+1)

    def gotosignup(self):
        signup = SignUpScreen()
        widget.addWidget(signup)
        widget.setCurrentIndex(widget.currentIndex() + 1)


class LoginScreen(QDialog):
    def __init__(self):
        super(LoginScreen, self).__init__()
        uic.loadUi("login.ui", self)
        #hide password
        self.passwordField.setEchoMode(QtWidgets.QLineEdit.Password)
        self.login.clicked.connect(self.loginfunction)
        self.back.clicked.connect(self.backfunction)

    def loginfunction(self):
        username = self.usernameField.text()
        password = self.passwordField.text()

        if len(username) == 0 or len(password) == 0:
            self.error.setText("Please input all fields.")
        else:
            result_username = database.check_if_user_exist(username)
            if result_username:
                result_pass = database.valid_password(username, password)
                if result_pass:
                    self.error.setText("Successfully logged in!")
                    self.gotomenu(username)
                else:
                    self.error.setText("Invalid username or password.")
            else:
                self.error.setText("Invalid username or password.")
    def backfunction(self):
        welcome = WelcomeScreen()
        widget.addWidget(welcome)
        widget.setCurrentIndex(widget.currentIndex() + 1)

    def gotomenu(self, username):
        menu = MenuScreen(username)
        widget.addWidget(menu)
        widget.setCurrentIndex(widget.currentIndex()+1)

class SignUpScreen(QDialog):
    def __init__(self):
        super(SignUpScreen, self).__init__()
        uic.loadUi("signup.ui", self)
        # hide password
        self.passwordField.setEchoMode(QtWidgets.QLineEdit.Password)
        self.confirmPasswordField.setEchoMode(QtWidgets.QLineEdit.Password)

        self.back.clicked.connect(self.backfunction)
        self.signup.clicked.connect(self.SignUpfunction)

    def SignUpfunction(self):
        username = self.usernameField.text()
        password = self.passwordField.text()
        confirm_password = self.confirmPasswordField.text()

        if len(username) == 0 or len(password) == 0 or len(confirm_password) == 0:
            self.error.setText("Please input all fields.")
        else:
            if confirm_password != password:
                self.error.setText("Passwords do not match.")
            elif database.check_if_user_exist(username):
                self.error.setText("User already exists.")
            else:
                database.insert_user_into_table(username, password, '0', '1', '2', '3', '4')
                login = LoginScreen()
                widget.addWidget(login)
                widget.setCurrentIndex(widget.currentIndex() + 1)
    def backfunction(self):
        welcome = WelcomeScreen()
        widget.addWidget(welcome)
        widget.setCurrentIndex(widget.currentIndex() + 1)


class MenuScreen(QDialog):
    def __init__(self, username):
        self.username = username
        super(MenuScreen, self).__init__()
        uic.loadUi("menu_ui.ui", self)
        self.start.clicked.connect(self.startfunction)
        self.edit.clicked.connect(self.editfunction)
        self.logout.clicked.connect(self.logoutfunction)


    def startfunction(self):
        global flag
        flag = True
        pygame.init()
        num_of_fingers = finger_detection_using_glove.run_camera(5)
        #handle with logout while running camera
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return
        # get user commands - [finger_one, finger_two...]
        try:
            if num_of_fingers == -1:
                raise
            if num_of_fingers > 4:
                num_of_fingers = 4
            user_commands = database.get_user_commands(self.username)
            database.take_action(self.username, user_commands[0][num_of_fingers])
        except:
            print("Error")
        flag = False
        pygame.quit()
    def editfunction(self):
        edit = EditScreen(self.username)
        widget.addWidget(edit)
        widget.setCurrentIndex(widget.currentIndex() + 1)
    def logoutfunction(self):
        global flag

        #handle with logout while running the camera
        if flag:
            pygame.event.post(pygame.event.Event(pygame.QUIT))
        welcome = WelcomeScreen()
        widget.addWidget(welcome)
        widget.setCurrentIndex(widget.currentIndex() + 1)

class EditScreen(QDialog):
    def __init__(self, username):
        self.username = username
        super(EditScreen, self).__init__()
        uic.loadUi("edit_commands.ui", self)
        for item in database.get_commands_table():
            self.command_zero.addItem(item[1])
        for item in database.get_commands_table():
            self.command_one.addItem(item[1])
        for item in database.get_commands_table():
            self.command_two.addItem(item[1])
        for item in database.get_commands_table():
            self.command_three.addItem(item[1])
        for item in database.get_commands_table():
            self.command_four.addItem(item[1])

        self.back.clicked.connect(self.backfunction)
        self.save.clicked.connect(self.savefunction)
        self.copy_button.clicked.connect(self.copyfunction)

    def copyfunction(self):
        copy_window = CopyScreen(self.username)
        widget.addWidget(copy_window)
        widget.setCurrentIndex(widget.currentIndex() + 1)
    def backfunction(self):
        menu = MenuScreen(self.username)
        widget.addWidget(menu)
        widget.setCurrentIndex(widget.currentIndex()+1)

    def savefunction(self):
        database.update_commands(self.command_zero.currentIndex(),
                                 self.command_one.currentIndex(),
                                 self.command_two.currentIndex(),
                                 self.command_three.currentIndex(),
                                 self.command_four.currentIndex(),
                                 self.username)
        self.saved_successfully.setText("geastures saved successfully!")

class CopyScreen(QDialog):
    def __init__(self, username):
        self.username = username
        super(CopyScreen, self).__init__()
        uic.loadUi("copy_geastures_screen.ui", self)

        for user in database.return_users():
            if user != username:
                self.users.addItem(user)

        self.save.clicked.connect(self.savefunction)
        self.back.clicked.connect(self.backfunction)
    def savefunction(self):
        print(self.username)
        print(self.users.currentText())
        database.copy_user_menu(self.username, self.users.currentText())
        self.saved_successfully.setText("geastures saved successfully!")

    def backfunction(self):
        edit_commands_screen = EditScreen(self.username)
        widget.addWidget(edit_commands_screen)
        widget.setCurrentIndex(widget.currentIndex()+1)
# Initialize the app
#main
app = QApplication(sys.argv)
welcome = WelcomeScreen()
widget = QStackedWidget()
widget.addWidget(welcome)
widget.setFixedWidth(600)
widget.setFixedHeight(480)
widget.show()

try:
    sys.exit(app.exec_())
except:
    print("Exiting")