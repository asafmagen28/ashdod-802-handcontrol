# import the opencv library
import colorsys
import cv2
import numpy as np
import sys
from skinDetector import SkinDetector
import database
import time
from PIL import Image


WHITE = 255
BLACK = 0
DIFFERENCE_DIVIDE = 16

WIDTH_START = 322
WIDTH_END = 598

HEIGHT_START = 102
HEIGHT_END = 438

def main():
    # define a video capture object
    w, h = 1920, 1080
    vid = cv2.VideoCapture(0)
    vid.set(3, 1080)
    vid.set(4, 1920)

    # print height and width

    width = vid.get(cv2.CAP_PROP_FRAME_WIDTH)
    height = vid.get(cv2.CAP_PROP_FRAME_HEIGHT)
    print(width, height)

    while (True):

        # Capture the video frame
        # by frame
        ret, frame = vid.read()
        cv2.rectangle(frame, (320, 100), (600, 440), (0, 255, 0), 2)

        # Display the resulting frame

        # save the hand area in array
        #take this range to ignore the rectangle draw
        hand_area = frame[HEIGHT_START:HEIGHT_END, WIDTH_START:WIDTH_END]

        face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
        # Convert to grayscale
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Detect faces
        """faces = face_cascade.detectMultiScale(gray, 1.1, 4)
        for (x, y, w, h) in faces:
            cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)

        #get the middle of the face

        middle_point_frame = frame[int((x + w) / 2), int((y + h) / 2)]
        rgb = np.uint8([[[middle_point_frame[0], middle_point_frame[1], middle_point_frame[2]]]])
        hsv = cv2.cvtColor(rgb, cv2.COLOR_RGB2HSV)

        # Print the range of HSV values
        print("Hue range:", [hsv[0][0][0] - 10, hsv[0][0][0] + 10])
        print("Saturation range:", [hsv[0][0][1] - 10, hsv[0][0][1] + 10])
        print("Value range:", [hsv[0][0][2] - 10, hsv[0][0][2] + 10])

        # Draw a rectangle around the faces"""

        # show image HSV
        imagehsv = cv2.cvtColor(hand_area, cv2.COLOR_BGR2HSV)
        lower_black = np.array([0, 25, 82])
        upper_black = np.array([255, 255, 255])

    #hue value by pixel in the middle of the face
        """hue_low = int(hsv[0][0][0] / 2)
        hue_high = int(hsv[0][0][0] * 2)

        saturation_low = int(hsv[0][0][1] / 2)
        saturation_high = int(hsv[0][0][1] * 2)

        value_low = int(hsv[0][0][2] / 2)
        value_high = int(hsv[0][0][2] * 2)

        low = np.array([hue_low, saturation_low, value_low])
        upp = np.array([hue_high, saturation_high, value_high])

        imagemask = cv2.inRange(imagehsv, lower_black, upper_black)
        mask = cv2.inRange(imagehsv, low, upp)
        cv2.imshow('frame1', imagemask)"""

        # show image using contours
        # Grayscale
        gray = cv2.cvtColor(hand_area, cv2.COLOR_BGR2GRAY)

        # Find Canny edges
        edged = cv2.Canny(gray, 30, 200)

        # Finding Contours
        # Use a copy of the image e.g. edged.copy()
        # since findContours alters the image
        contours, hierarchy = cv2.findContours(edged,
                                               cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

        # show the hand area with Contours
        cv2.imshow('frame2', edged)

        ret, thresh_img = cv2.threshold(gray, 140, 255, cv2.THRESH_BINARY_INV)

        #show the Output image on the newly created image window
        cv2.imshow('Output', thresh_img)
        # the 'q' button is set as the
        # quitting button you may use any
        # desired button of your choice


        width_hand = WIDTH_END - WIDTH_START
        height_hand = HEIGHT_END - HEIGHT_START
        difference = height_hand / DIFFERENCE_DIVIDE
        highest = 0
        difference_between_pixels = 0
        points_recognized = []

        #loop througth the frame and to count the number of upper fingers using he difference between the pixels
        for y in range(0, height_hand, int(difference)):
            counter = 0
            difference_between_pixels = 0
            curr_pixel = thresh_img[y, 0]
            for x in range(width_hand):
                # checks if the previous and the current pixels are different
                if thresh_img[y, x] != curr_pixel:
                    counter = counter + 1
                    #difference_between_pixels = 0
                curr_pixel = thresh_img[y, x]
            if(counter > highest):
                highest = counter
            if (counter > 1):
                points_recognized.append(counter)

        try:
            cv2.putText(frame, str(int(points_recognized[3] / 2)), (45, 375), cv2.FONT_HERSHEY_PLAIN,
                      10, (255, 0, 0), 20)
        except:
            cv2.putText(frame, "len:" + str(len(points_recognized)), (45, 375), cv2.FONT_HERSHEY_PLAIN,
                        10, (255, 0, 0), 20)
        cv2.imshow('original frame', frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break


    # After the loop release the cap object
    vid.release()
    # Destroy all the windows
    cv2.destroyAllWindows()


def skin_detection(frame):
    # Convert the input image to the HSV color space
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # Define the skin color range in the HSV space
    lower_skin = np.array([0, 20, 70], dtype=np.uint8)
    upper_skin = np.array([20, 255, 255], dtype=np.uint8)

    # Threshold the image based on the skin color range
    mask = cv2.inRange(hsv, lower_skin, upper_skin)

    # Apply morphological operations to remove noise and small non-skin regions
    kernel = np.ones((5, 5), np.uint8)
    mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)

    return mask

if __name__ == "__main__":
    main()